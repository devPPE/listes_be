import requests

# Remplacez ces valeurs par vos propres informations
url_login = "http://localhost:8000/auth/login/"
credentials = {
    "username": "ppe",
    "password": "8729"
}

# Envoi de la requête de connexion pour obtenir un token
response = requests.post(url_login, data=credentials)
print(response.json())
# Vérification si la connexion est réussie
if response.status_code == 200:
    # Extraction du token d'authentification
    token = response.json().get('key')
    print("Token d'authentification obtenu :", token)
else:
    print("Erreur de connexion :", response.status_code)