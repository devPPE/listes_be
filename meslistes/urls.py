from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import TacheViewSet

# Création d'un router et enregistrement de nos viewsets
router = DefaultRouter()
router.register(r'taches', TacheViewSet)

# Les URL de l'API sont maintenant déterminées automatiquement par le router
urlpatterns = [
    path('', include(router.urls)),
]