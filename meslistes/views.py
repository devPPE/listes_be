from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from .models import Tache
from .serializers import TacheSerializer
from django.contrib.auth import get_user_model
from rest_framework import generics,permissions
from .serializers import UserSerializer

User = get_user_model()

class UserCreate(generics.CreateAPIView):
    permission_classes = [permissions.AllowAny]
    authentication_classes = ()
    serializer_class = UserSerializer
    queryset = User.objects.all()

class TacheViewSet(viewsets.ModelViewSet):
    """
    Un viewset pour voir, ajouter, modifier ou supprimer des tâches.
    """
    queryset = Tache.objects.all()
    serializer_class = TacheSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """
        Cette vue ne devrait retourner que les tâches de l'utilisateur connecté.
        """
        user = self.request.user
        return Tache.objects.filter(utilisateur=user)
    def perform_create(self, serializer):
        """
        Associe automatiquement l'utilisateur authentifié à la tâche.
        """
        serializer.save(utilisateur=self.request.user)