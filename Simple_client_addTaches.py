# Assurez-vous d'avoir obtenu un token valide à l'étape précédente
import requests

url_login = "http://localhost:8000/auth/login/"
credentials = {
    "username": "ppe",
    "password": "8729"
}

# Envoi de la requête de connexion pour obtenir un token
response = requests.post(url_login, data=credentials)
print(response.json())
# Vérification si la connexion est réussie
if response.status_code == 200:
    # Extraction du token d'authentification
    token = response.json().get('key')
    print("Token d'authentification obtenu :", token)
else:
    print("Erreur de connexion :", response.status_code)

print("token" + token)
headers = {
    "Authorization": "Token {token}".format(token=token)
}


# Remplacez cette URL par l'endpoint spécifique de votre API pour ajouter une tâche
url_add_tache = "http://localhost:8000/api/taches/"

# Les données de la nouvelle tâche à ajouter
tache_data = {
    "titre": "Titre de la tâche",
    "description": "Description de la tâche",
    # Ajoutez d'autres champs requis par votre modèle de tâche ici
}
print(tache_data)

# Envoi de la requête pour ajouter une nouvelle tâche
response = requests.post(url_add_tache, headers=headers, data=tache_data)
# response = requests.get(url_add_tache, headers=headers)

# Vérification si l'ajout de la tâche est réussi
if response.status_code == 201:
    print("Tâche ajoutée avec succès :", response.json())
else:
    print("Erreur lors de l'ajout de la tâche :", response.status_code)
    print(response)
